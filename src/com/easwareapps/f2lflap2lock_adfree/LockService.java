/*  F2L Flap 2 Lock is an android application to lock the phone using proximity sensor

    Copyright (C) 2014  Vishnu V vishnu@easwareapps.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.easwareapps.f2lflap2lock_adfree;



import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


public class LockService extends Service  implements SensorEventListener, Runnable{
	
	Thread lockThread = null;

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		try{
			stopForeground(true);
			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			try{
				notificationManager.cancel(456);
			}catch(Exception e){
				e.printStackTrace();
			}
			proximitySensor.unregisterListener(this);
		}catch(Exception e){

		}
		super.onDestroy();
	}

	static long unlockeTime = 0;

	float lastRoll = 0;
	Context context;
	private SensorManager proximitySensor = null;
	SharedPreferences f2lPref = null;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		context = this.getApplicationContext();
		f2lPref = this.getSharedPreferences("com.easwareapps.f2lflap2lock", Context.MODE_PRIVATE);
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.intent.action.USER_PRESENT");
		registerReceiver(receiver, filter);
		if(lockThread == null){
			lockThread = new Thread(this);
		}
		super.onCreate();
	}





	IBinder mBinder = new LocalBinder();

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	public class LocalBinder extends Binder {
		public LockService getServerInstance() {
			return LockService.this;
		}

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if(!f2lPref.getBoolean("enable_f2l", true)){
			return;
		}
		
		float distance = event.values[0];
		if(event.sensor.getType() == Sensor.TYPE_PROXIMITY){
			if(distance < event.sensor.getMaximumRange() ){
				
				if(!lockThread.isAlive()){
					try{
						lockThread.start();
					}catch (Exception e) {
						// TODO: handle exception
						try{
							lockThread = new Thread(this);
							lockThread.start();
						}catch (Exception e1) {
							// TODO: handle exception
							Log.d("EXE", "Can't start thread");
						}
					}
				}
				return;
				
			}
		}
		if(lockThread.isAlive()){
			try {
				lockThread.interrupt();
				lockThread.join();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
		
	private void lockScreen(){
		
		if(f2lPref.getBoolean("disable_lock_after_unlock_tmp", true)){
			int allowedTimeDiff = f2lPref.getInt("seek_time", 5)*1000;
			if(( System.currentTimeMillis() - unlockeTime ) <= allowedTimeDiff ){
				return;
			}
		}
		if(f2lPref.getBoolean("disable_lock_on_call", true)){
			TelephonyManager telManager = (TelephonyManager) 
					context.getSystemService(Context.TELEPHONY_SERVICE);
			if(telManager.getCallState() != 
					TelephonyManager.CALL_STATE_IDLE && !( isHeadsetConnected()
					&&  f2lPref.getBoolean("enable_lock_when_headset_connected", true))){
				return;

			}
		}
		if(isConnected(context)
				&& f2lPref.getBoolean("disable_lock_when_charger_connected", false)){
			return;
		}
			
		DevicePolicyManager mDPM  = (DevicePolicyManager)getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
		ComponentName mAdminName = new ComponentName(this, AdminManageReceiver.class);
		if(!mDPM.isAdminActive(mAdminName)){
			Toast.makeText(context, (String) getResources().getText(R.string.desc_cant_lock), Toast.LENGTH_SHORT).show();
		}else{
			mDPM.lockNow();
		}

	}

	public void startSensorListening(){
		setNotification(f2lPref.getBoolean("enable_f2l", true));
		if(proximitySensor != null)	return;
		proximitySensor = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		//mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

		proximitySensor.registerListener(this,
				proximitySensor.getDefaultSensor(Sensor.TYPE_PROXIMITY),
				SensorManager.SENSOR_DELAY_UI);
	}

	public void stopSensorListening(){

		try{
			proximitySensor.unregisterListener(this);
			proximitySensor = null;
		}catch(Exception e){

		}

	}




	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		if(f2lPref==null){
			f2lPref = this.getSharedPreferences("com.easwareapps.f2lflap2lock", Context.MODE_PRIVATE);
		}
		boolean notNeeded = true;
		try{
			if(intent.getExtras() != null){

				Bundle b = intent.getExtras();
				if(b.getBoolean("UNLOCKED")){
					notNeeded = false;
				}else if(b.getBoolean("BOOT_COMPLETED")){
					if(f2lPref.getBoolean("enable_f2l_on_boot", true)){
						if(f2lPref.getBoolean("enable_f2l", true)){
							startSensorListening();
							notNeeded = true;
						}
					}
				}
			}
		}catch(Exception e){

		}
		if(notNeeded){
			setNotification(f2lPref.getBoolean("enable_f2l", true));
		}
		super.onStartCommand(intent, flags, startId);
		return START_STICKY;
	}

	private void setNotification(boolean value) {
		// TODO Auto-generated method stub

		
		Intent intentS = new Intent(getApplicationContext(), MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intentS, 0);
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		try{
			stopForeground(true);
			notificationManager.cancel(456);
		}catch(Exception e){
			e.printStackTrace();
		}
		String msg = (String) getResources().getText(R.string.title_f2l_disabled);
		int id = R.drawable.ic_disable;
		if(value){
			msg = (String) getResources().getText(R.string.title_f2l_enabled); 
			id = R.drawable.ic_launcher;
		}
		
		Notification noti = new NotificationCompat.Builder(this)
		.setContentTitle("F2L")
		.setContentText(msg).setSmallIcon(id)
		.setContentIntent(pIntent)
		.build();
		startForeground(456, noti);

	}

	private boolean isConnected(Context context) {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }
	
	@SuppressWarnings("deprecation")
	private boolean isHeadsetConnected(){
		AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
		return am.isWiredHeadsetOn() | am.isBluetoothA2dpOn();
	}
	

	private final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(action.equals("android.intent.action.USER_PRESENT")){
				unlockeTime = System.currentTimeMillis();
			}
		}
	};

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			f2lPref = this.getSharedPreferences("com.easwareapps.f2lflap2lock", Context.MODE_PRIVATE);
			int milliSeconds = f2lPref.getInt("wait_for_after_proximity_changed", 0)*1000;
			Thread.sleep(milliSeconds);
			lockScreen();
		}catch(InterruptedException e){
			Log.d("run", "intrrupted");
		}
		
	}


}
