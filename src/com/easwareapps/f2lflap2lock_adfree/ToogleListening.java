/*  F2L Flap 2 Lock is an android application to lock the phone using proximity sensor

    Copyright (C) 2014  Vishnu V vishnu@easwareapps.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.easwareapps.f2lflap2lock_adfree;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.WindowManager;


public class ToogleListening extends BroadcastReceiver{




	WindowManager wmgr = null;
	String PUBLISHER_ID="4e7edf570a1703062c0092faf0f4996e";

	boolean mBounded = false;
	LockService mServer = null;
	String command = "";
	Context c = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.d("TL", intent.getAction().toString());
		c = context;

			//do something here

			Log.d("NULL", "START");
			command = "START";
			Intent serviceIntent = new Intent(context, LockService.class);
			serviceIntent.putExtra("BOOT_COMPLETED", true);
			context.startService(serviceIntent);




	}
}
