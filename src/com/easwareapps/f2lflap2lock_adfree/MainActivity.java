/*  F2L Flap 2 Lock is an android application to lock the phone using proximity sensor

    Copyright (C) 2014  Vishnu V vishnu@easwareapps.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.easwareapps.f2lflap2lock_adfree;

import java.security.Policy;


import com.easwareapps.f2lflap2lock_adfree.R;
import com.easwareapps.f2lflap2lock_adfree.LockService;
import com.easwareapps.f2lflap2lock_adfree.LockService.LocalBinder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;




public class MainActivity extends Activity implements OnCheckedChangeListener, OnClickListener, OnSeekBarChangeListener{

	Policy mPolicy = null;

	boolean mBounded = false;
	LockService mServer = null;

	final static int ENABLE_ADMIN = 1;
	final static int SUCESS = -1;

	SharedPreferences f2lPref = null;
	SharedPreferences.Editor prefEditor = null;

	TextView txtEnableAdmin = null;
	TextView txtWaitBeforeLock = null;
	TextView txtHelp = null;
	TextView txtExit = null;
	TextView txtRefer = null;


	CheckBox chkEnableF2L = null;
	CheckBox chkEnableOnBoot = null;
	CheckBox chkEnableQuickChanger = null;
	CheckBox chkDisableOnCall = null;
	CheckBox chkDisableAfterUnlock = null;
	CheckBox chkEnableWhenHeadsetConnected = null;
	CheckBox chkDisableWhenChargerConnected = null;

	SeekBar seekTime = null;
	SeekBar seekWait = null;

	ComponentName mAdminName = null;

	public final void onCreate(Bundle savedInstanceState) {

		f2lPref = this.getSharedPreferences("com.easwareapps.f2lflap2lock", MODE_PRIVATE);
		prefEditor = f2lPref.edit();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		chkEnableF2L = (CheckBox)findViewById(R.id.idEnableF2L);
		chkEnableOnBoot = (CheckBox)findViewById(R.id.idEnableOnBoot);
		chkDisableOnCall = (CheckBox)findViewById(R.id.idDisableOnCall);
		chkDisableAfterUnlock = (CheckBox)findViewById(R.id.idDisableAfterLock);
		seekTime = (SeekBar)findViewById(R.id.idTimeSeek);
		seekWait = (SeekBar)findViewById(R.id.idWaitFor);
		chkDisableWhenChargerConnected = (CheckBox)findViewById
				(R.id.idDisableIfChargerConnected);
		chkEnableWhenHeadsetConnected = (CheckBox)findViewById
				(R.id.idEnableIfHeadsetConnected);


		txtEnableAdmin = (TextView)findViewById(R.id.idEnableAdmin);
		txtWaitBeforeLock = (TextView)findViewById(R.id.idWaitBeforeLock);
		txtHelp = (TextView)findViewById(R.id.idHelp);
		txtExit = (TextView)findViewById(R.id.idExit);
		txtRefer = (TextView)findViewById(R.id.idRefer);

		setPreference();

		txtEnableAdmin.setOnClickListener(this);
		txtHelp.setOnClickListener(this);
		txtExit.setOnClickListener(this);
		txtRefer.setOnClickListener(this);

		chkEnableF2L.setOnCheckedChangeListener(this);
		chkEnableOnBoot.setOnCheckedChangeListener(this);
		chkDisableAfterUnlock.setOnCheckedChangeListener(this);
		chkDisableOnCall.setOnCheckedChangeListener(this);
		chkDisableWhenChargerConnected.setOnCheckedChangeListener(this);
		chkEnableWhenHeadsetConnected.setOnCheckedChangeListener(this);

		seekTime.setOnSeekBarChangeListener(this);
		seekWait.setOnSeekBarChangeListener(this);
		

		mAdminName = new ComponentName(this, AdminManageReceiver.class);
		DevicePolicyManager mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
		if(!mDPM.isAdminActive(mAdminName)){
			enableChildVisibility(false);
			showAdminManagement();

		}else{
			Log.d("123", "hello");
			startLockService();
			enableChildVisibility(true);
			
		}
		
		
	}
	private void showAdminManagement() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
		intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
		intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, 
				R.string.desc_enable_admin);
		startActivityForResult(intent, ENABLE_ADMIN);

	}



	private void enableChildVisibility(boolean val) {
		// TODO Auto-generated method stub
		int visibility = (val)?View.GONE:View.VISIBLE;
		txtEnableAdmin.setVisibility(visibility);

		chkEnableF2L.setEnabled(val);
		chkEnableOnBoot.setEnabled(val);
		chkDisableOnCall.setEnabled(val);
		chkDisableAfterUnlock.setEnabled(val);
		chkDisableWhenChargerConnected.setEnabled(val);
		if(val && f2lPref.getBoolean("disable_lock_after_unlock_tmp", true))
			seekTime.setEnabled(true);
		else	seekTime.setEnabled(false);
		if(val && f2lPref.getBoolean("disable_lock_on_call", true))
			chkEnableWhenHeadsetConnected.setEnabled(true);
		else	chkEnableWhenHeadsetConnected.setEnabled(false);

	}

	private void setPreference() {
		// TODO Auto-generated method stub
		boolean value = f2lPref.getBoolean("enable_f2l", true);
		chkEnableF2L.setChecked(value);

		value = f2lPref.getBoolean("enable_f2l_on_boot", false);
		chkEnableOnBoot.setChecked(value);

		value = f2lPref.getBoolean("disable_lock_on_call", true);
		chkDisableOnCall.setChecked(value);
		
		chkEnableWhenHeadsetConnected.setEnabled(value);
		value = f2lPref.getBoolean("enable_lock_when_headset_connected", true);
		chkEnableWhenHeadsetConnected.setChecked(value);


		value = f2lPref.getBoolean("disable_lock_after_unlock_tmp", true);
		chkDisableAfterUnlock.setChecked(value);


		int wait = f2lPref.getInt("wait_for_after_proximity_changed", 0);
		if(wait == 0){
			txtWaitBeforeLock.setText((String)getResources().getText(R.string.title_lock_immediately)); 
		}else{
			txtWaitBeforeLock.setText((String)getResources().getText(R.string.title_wait_before_lock)
				+ " " + wait + " " + (String)getResources().getText(R.string.title_seconds));
		}
		seekWait.setProgress(wait);
		
		int time = f2lPref.getInt("seek_time", 5);
		seekTime.setProgress(time);
		seekTime.setEnabled(value);
		chkDisableAfterUnlock.setText( (String)getResources().getText(R.string.title_disable_lock_for) 
				+ time + (String)getResources().getText(R.string.title_once_unlocked));

		value = f2lPref.getBoolean("disable_lock_when_charger_connected", false);
		chkDisableWhenChargerConnected.setChecked(value);
		
		prefEditor.commit();
	}



	private void startLockService(){
		if(mServer != null){			
			mServer.startSensorListening();
		}else{
			Intent serviceIntent = new Intent(MainActivity.this, LockService.class);
			if(mServer == null){
				startService(serviceIntent);
				bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);

			}
		}


	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		Log.d("RESULT", requestCode+"|"+resultCode);
		if(requestCode == ENABLE_ADMIN && resultCode == SUCESS){
			enableChildVisibility(true);
			startLockService();
		}else{
			enableChildVisibility(false);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}


	ServiceConnection mConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {

			mBounded = false;
			mServer = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			//Toast.makeText(FloatingWidgetService.this, "Service is connected", 1000).show();
			mBounded = true;
			LocalBinder mLocalBinder = (LocalBinder)service;
			mServer = mLocalBinder.getServerInstance();
			mServer.startSensorListening();


		}




	};

	@Override
	public void onCheckedChanged(CompoundButton view, boolean value) {
		// TODO Auto-generated method stub
		Log.d("value", ""+value);
		if(view == chkEnableF2L){

			prefEditor.putBoolean("enable_f2l", value);
			prefEditor.commit();
			startLockService();
			return;

		}else if(view == chkEnableOnBoot){

			prefEditor.putBoolean("enable_f2l_on_boot", value);

		}else if(view == chkDisableAfterUnlock){

			prefEditor.putBoolean("disable_lock_after_unlock_tmp", value);
			seekTime.setEnabled(value);

		}else if(view == chkDisableOnCall){
			chkEnableWhenHeadsetConnected.setEnabled(value);
			prefEditor.putBoolean("disable_lock_on_call", value);

		}else if(view == chkDisableWhenChargerConnected){
			
			prefEditor.putBoolean("disable_lock_when_charger_connected", value);
			
		}else if(view == chkEnableWhenHeadsetConnected){
			prefEditor.putBoolean("enable_lock_when_headset_connected", value);
		}
		prefEditor.commit();

	}




	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try{
			unbindService(mConnection);
			mBounded = false;
			mServer = null;
		}catch(Exception e){

		}
		super.onStop();
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view == txtEnableAdmin){
			showAdminManagement();
		}else if(view == txtHelp){
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("http://f2l.easwareapps.com#help"));
			startActivity(intent);
		}else if(view == txtExit){
			try{
				Intent service =new Intent(this, LockService.class);
				stopService(service);

			}catch (Exception e) {
				// TODO: handle exception
			}
			NotificationManager notificationManager = (NotificationManager) 
					getSystemService(NOTIFICATION_SERVICE);
			try{
				notificationManager.cancel(456);
			}catch(Exception e){

			}
			System.exit(0);
		}else if(view == txtRefer){			
			String msg = "Check out F2L Flap2Lock, Lock phone using proximity sensor.\nFor more details http://f2l.easwareapps.com";
			Intent intent=new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			intent.putExtra(Intent.EXTRA_SUBJECT, "F2L Flap2Lock");
			intent.putExtra(Intent.EXTRA_TEXT, msg);
			startActivity(Intent.createChooser(intent, (String) getResources().getText(R.string.title_how_share)));
		}

	}



	@Override
	public void onProgressChanged(SeekBar view, int value, boolean arg2) {
		// TODO Auto-generated method stub
		if(view == seekTime){
			chkDisableAfterUnlock.setText((String)getResources().getText(R.string.title_disable_lock_for) 
					+ value + (String)getResources().getText(R.string.title_once_unlocked));
			prefEditor.putInt("seek_time", value);
			prefEditor.commit();
		}else if(view == seekWait){
			if(value == 0){
				txtWaitBeforeLock.setText((String)getResources().getText(R.string.title_lock_immediately)); 
			}else{
				//value = 
				txtWaitBeforeLock.setText((String)getResources().getText(R.string.title_wait_before_lock)
				+ " " + value + " " + (String)getResources().getText(R.string.title_seconds));
			}
			prefEditor.putInt("wait_for_after_proximity_changed", value);
			prefEditor.commit();
		}

	}



	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub

	}



	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub

	}

	public void buyF2L(View view){

		String[] items = {(String) getResources().getText(R.string.play_store),
				(String) getResources().getText(R.string.opera_store),
				(String) getResources().getText(R.string.other_store)};
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder.setTitle(R.string.title_purchase_from);

		alertDialogBuilder.setItems(items, new DialogInterface.OnClickListener() {
			String url = "https://play.google.com/store/apps/details?id=com.easwareapps.f2lflap2lock_adfree";
			public void onClick(DialogInterface dialog, int index) {
				switch (index) {
				case 0:

					url = "https://play.google.com/store/apps/details?id=";
					openStore(url, true);
					break;
				case 1:
					url = "http://apps.opera.com/en_in/f2l_flap_2_lock_ad_free.html";
					openStore(url, false);
					break;
				default:
					url = "http://f2l.easwareapps.com#buy";
					openStore(url, false);
					break;
				}

			}
		});

		alertDialogBuilder.setNegativeButton(R.string.cancel,  null).setCancelable(true);	
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}
	
	public void openStore(String url, boolean playstore){
		String packageName = "com.easwareapps.f2lflap2lock_adfree";
		if(playstore){
			try {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url + packageName)));
			} catch (android.content.ActivityNotFoundException anfe) {
				url  = "market://details?id=" + packageName;
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			}
		}else{
			
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
		}
	}






}
